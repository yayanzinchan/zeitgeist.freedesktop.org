---
layout: default
title: Download
---

# Download

## Source Release

 * [You can download the latest version of Zeitgeist (1.0.1) from Launchpad](https://launchpad.net/zeitgeist/1.0/1.0.1)
 * [See older releases](https://launchpad.net/zeitgeist/+download)

## Development Version

Go to the [development page](/development/) for more information.
